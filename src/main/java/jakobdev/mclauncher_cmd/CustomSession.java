package main.java.jakobdev.mclauncher_cmd;

import sk.tomsik68.mclauncher.api.login.ESessionType;
import sk.tomsik68.mclauncher.api.login.ISession;

import java.util.List;

public class CustomSession implements ISession {

    @Override
    public String getUsername() {
        return "{username}";
    }

    @Override
    public String getSessionID() {
        return "{accesToken}";
    }

    @Override
    public String getUUID() {
        return "{uuid}";
    }

    public ESessionType getType() {
        return ESessionType.MOJANG;
    }

    public List<Prop> getProperties() {
        return null;
    }
}
