package main.java.jakobdev.mclauncher_cmd;

import sk.tomsik68.mclauncher.backend.MinecraftLauncherBackend;

import java.io.File;

public class mclauncher_cmd {
    public static void main(String[] args) throws Exception {
        if (args.length != 3) {
            System.out.println("Usage: mclauncher-cmd [command|install] <version> <directory>");
            System.exit(1);
        }
        MinecraftLauncherBackend minecraftLauncherBackend = new MinecraftLauncherBackend(new File(args[2]));
        if (args[0].equals("command")) {
            CustomSession session = new CustomSession();
            LaunchSettings settings = new LaunchSettings();
            ProcessBuilder pb = minecraftLauncherBackend.launchMinecraft(session, null, args[1], settings, null);
            String command = pb.command().toString();
            command = command.replaceAll(",","");
            command = command.replaceAll("--demo ","");
            command = command.substring(1, command.length() - 1);
            System.out.println(command);
        }
        else if (args[0].equals("install")) {
            ProgressPrint progress = new ProgressPrint();
            minecraftLauncherBackend.updateMinecraft(args[1], progress);
        }
        else {
            System.out.println("Usage: mclauncher-cmd [command|install] <version> <directory>");
            System.exit(1);
        }
    }
}
