package main.java.jakobdev.mclauncher_cmd;

import sk.tomsik68.mclauncher.api.common.ILaunchSettings;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class LaunchSettings implements ILaunchSettings {

    @Override
    public String getInitHeap() {
        return "512M";
    }

    @Override
    public String getHeap() {
        return "512M";
    }

    @Override
    public Map<String, String> getCustomParameters() {
        return null;
    }

    @Override
    public List<String> getCommandPrefix() {
        List<String> params = new ArrayList<String>();
        return params;
    }

    @Override
    public boolean isModifyAppletOptions() {
        return false;
    }

    @Override
    public File getJavaLocation() {
        return null;
    }

    @Override
    public List<String> getJavaArguments() {
        List<String> params = new ArrayList<String>();
        return params;
    }
}
