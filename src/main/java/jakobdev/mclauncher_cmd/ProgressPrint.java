package main.java.jakobdev.mclauncher_cmd;

import sk.tomsik68.mclauncher.api.ui.IProgressMonitor;

public class ProgressPrint implements IProgressMonitor {

    @Override
    public void setProgress(int i){
        System.out.println("setProgress#mclauncher-cmd#" + i);
    }

    @Override
    public void setMax(int i) {
        System.out.println("setMax#mclauncher-cmd#" + i);
    }

    @Override
    public void incrementProgress(int i) {
        System.out.println("incrementProgress#mclauncher-cmd#" + i);
    }

    @Override
    public void setStatus(String text) {
        System.out.println("setStatus#mclauncher-cmd#" + text);
    }
}
