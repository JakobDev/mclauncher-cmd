# mclauncher-cmd

mclauncher-cmd provide a commandline access for [mclauncher-api by tomsik68](https://github.com/tomsik68/mclauncher-api)

## Compile
You need to add mclauncher-api from [here](https://github.com/tomsik68/maven-repo/tree/master/sk/tomsik68/mclauncher-api) here and JSOn Smart 1.1 from [here](http://www.java2s.com/Code/Jar/j/Downloadjsonsmart11jar.htm) to your IDE.

## Usage
### Install a new Minecraft Version
To Install a new Minecraft Version run:  
`java -jar mclauncher-cmd.jar install <version> <folder>`  
Here is a example for installing Minecraft 1.14 in /home/User/.minecraft  
`java -jar mclauncher-cmd.jar install 1.14 /home/User/.minecraft`  
### Print Command to run Minecraft  
This will print a command to run Minecraft:  
`java -jar mclauncher-cmd.jar command <version> <folder>`  
Here is a example to print to command for running Minecraft 1.14 in /home/User/.minecraft  
`java -jar mclauncher-cmd.jar command 1.14 /home/User/.minecraft`  
You need to edit things like Username or Access Token before running.  
